<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * below creates a relationship between the permission and role models
 *
 */

class Permission extends Model
{
    public function roles() {
        return $this->belongsToMany(Role::class);
    }
}