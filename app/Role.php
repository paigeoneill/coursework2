<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * bellow creates a relationship between the role and permission models
 *
 *
 */

class Role extends Model
{
    public function permissions() {
        return $this->belongsToMany(Permission::class);
    }

    public function givePermissionTo(Permission $permission) {
        return $this->permssion()->sync($permission);
    }
}