<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Questionnaire;

use App\Question;

class QuestionnaireController extends Controller
{

    /*
   * Secure the set of pages to the admin.
   */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the questionnaires
        $questionnaires = Questionnaire::all();

        return view('admin/questionnaires', ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/questionnaires/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $questionnaire = Questionnaire::create($request->all());
        $questionnaire->user_id = Auth::user()->id;
        $questionnaire->questions()->attach($request->input('question'));
        $questionnaire->save();

        return redirect('admin/questionnaires');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the questionnaire
        $questionnaire = Questionnaire::with('user', 'questions')->where('id',$id)->first();

        // if questionnaire does not exist return to list
        if(!$questionnaire)
        {
            return redirect('/admin/questionnaires');
        }
        return view('/admin/questionnaires/show')->withQuestionnaire($questionnaire);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
