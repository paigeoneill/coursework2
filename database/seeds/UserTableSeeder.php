<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // add a known user

        DB::table('users')->insert([
            ['id' => 1, 'name' => "paige",
                'email' => 'paige@example.com',
                'password' => bcrypt('password'),
                'remember_token' => str_random(10),
            ],
        ]);

    }
}