-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: coursework2
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2017_04_27_235547_create_roles_table',1),('2017_04_27_235636_create_permissions_table',1),('2017_04_27_235714_create_questionnaire_table',1),('2017_04_27_235740_create_answers_table',1),('2017_04_27_235841_create_role_user_table',1),('2017_04_27_235913_create_permission_role_table',1),('2017_04_28_002258_create_questions_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaires`
--

DROP TABLE IF EXISTS `questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaires` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `questionnaires_published_at_index` (`published_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaires`
--

LOCK TABLES `questionnaires` WRITE;
/*!40000 ALTER TABLE `questionnaires` DISABLE KEYS */;
/*!40000 ALTER TABLE `questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `choices` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `questionnaire_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'paige','paige@example.com','$2y$10$aW4U7omTHz/st2rM7U13M.Y0ytS5ga7X173d..OisyXZiF5Lvk4SO','yRbdkvdY7r',NULL,NULL),(2,'Creola Purdy','paxton.kshlerin@example.org','$2y$10$/MELDAPm9aYWkeUbbyv7n.pIuV3wQiR.iPSDh4ZyOq6w0xVcKvXeq','TEN16xuClG','2017-04-30 22:38:54','2017-04-30 22:38:54'),(3,'Alf Donnelly','laura80@example.com','$2y$10$AtFGfG9uIoDGU0NAYEirc.nzqvzPnxMqxK6m/bkpHU/ICNn/gkg.q','vba19JUOYh','2017-04-30 22:38:54','2017-04-30 22:38:54'),(4,'Josh Fritsch','ngleason@example.net','$2y$10$9cPJShUialRtwObLuz.69.YTbdrJF0drn65F94ZuYqYvIgqFzikA.','ArKgFqDCMw','2017-04-30 22:38:54','2017-04-30 22:38:54'),(5,'Eva Gorczany','zgrimes@example.org','$2y$10$3VMKTRjFZ4fbkl6awz/NIeYuFDEltjD1giwamCkQjcHa.mxo1k/lO','rLWsYReFsw','2017-04-30 22:38:54','2017-04-30 22:38:54'),(6,'Caterina Schoen','wrau@example.org','$2y$10$20XcAfixRJCE.LjeEqyyhuRp192nXO05XDP3xv7sZl3ceUJg/PBW6','BMMiIArllp','2017-04-30 22:38:54','2017-04-30 22:38:54'),(7,'Wanda Romaguera','ikohler@example.org','$2y$10$SNKahFnu7lRwIJVScCtdRewWUTchq7PY/5pycSWXnfQExrY.IINW.','ZMNSGPSiKl','2017-04-30 22:38:54','2017-04-30 22:38:54'),(8,'Adonis Gislason V','maybell.schaden@example.net','$2y$10$7E6uJssmH26ACDdSBJ1GVe1/E1W/cC1UmIfU.4kJoZsDuzR.3TycK','2k6x6VnHvl','2017-04-30 22:38:54','2017-04-30 22:38:54'),(9,'Dr. Benedict Tremblay Jr.','carole75@example.com','$2y$10$v0KG41FJYBpIontcSUYvjeTClnyYkPPKc264cT4EHRoUF2tyC5UcG','8EWCNDzGNY','2017-04-30 22:38:54','2017-04-30 22:38:54'),(10,'Miss Lessie Jones PhD','katheryn.kautzer@example.org','$2y$10$PXWEF5u6GrmQWe3gaTCDGuWOOnddz0lonEql5aHf3Mr23zZcD8OG6','18Ct7GhEwC','2017-04-30 22:38:54','2017-04-30 22:38:54'),(11,'Dina Nikolaus','sedrick.frami@example.com','$2y$10$4thuNkTF8fuSg1iJx7CWkeaqKfTG3hvbiDkaGn.7lShgqXxUO6yVW','QRp0PW7oJq','2017-04-30 22:38:54','2017-04-30 22:38:54'),(12,'Benedict Runte','conn.imani@example.net','$2y$10$VcDmoiaYcUlMWTQsV8VbKuYdkJQ5UQrGzX/tyQfdhpHpdGpFzAtcy','tf0b6u2kf8','2017-04-30 22:38:54','2017-04-30 22:38:54'),(13,'Nicole Okuneva','deckow.myles@example.net','$2y$10$SKLzjuXrEMFick7vjiP.qeIF13kVaOWpb8y2dgT3zmKtDOekkHtMi','k0JZ9sewoz','2017-04-30 22:38:54','2017-04-30 22:38:54'),(14,'Bethel D\'Amore','dlueilwitz@example.net','$2y$10$O8gAdEPw4wjgUda3wgBgUebuwjRZ45qcUSdmO5pjJFhe0Ts.BDsgu','9McN0YUlOR','2017-04-30 22:38:54','2017-04-30 22:38:54'),(15,'Casimer Cormier I','gledner@example.org','$2y$10$v1GGI2qZ5Euz7w4/ofQn5.pk0rZSStic.YbT6dSZbIHAgCDMRQBdW','k1SG35xJGq','2017-04-30 22:38:54','2017-04-30 22:38:54'),(16,'Cynthia Lehner','delta95@example.org','$2y$10$QeEY4lf8d/jxFg62yp3gVuZloMuOf9cQH9Q0mh2axp/99ZSGVD3Te','5v06sjZAL3','2017-04-30 22:38:54','2017-04-30 22:38:54'),(17,'Dr. Elva Berge Jr.','kassulke.hershel@example.com','$2y$10$fXW0aiLWNd.sKGwjn6bZgOU6MOLLMIFzp2yeBqil3N/DqqG6lZy8i','62zWNykIZw','2017-04-30 22:38:54','2017-04-30 22:38:54'),(18,'Richard Kub Sr.','wlebsack@example.net','$2y$10$oEoP.GkLPuYok7CmNBpXrONRyqfIFIWy8IHFe/Lx1szFP4xh8Cqd.','m5DFzGGbfA','2017-04-30 22:38:54','2017-04-30 22:38:54'),(19,'Clark Baumbach','toy.marcia@example.org','$2y$10$JZFHo860tvxSvWiPRAnGZ.8pBBAOYodtwxWCY6nOaXjh6/1ixjsw2','wiTWJir4y0','2017-04-30 22:38:54','2017-04-30 22:38:54'),(20,'Imelda Block Jr.','sziemann@example.com','$2y$10$sdbp9.Ju5ITI42w0KW.qIeb3sVUuBIkbqf6Y.ca6o5jDQYRA1ySKC','7ckjcpqNaH','2017-04-30 22:38:54','2017-04-30 22:38:54'),(21,'Prof. Joey Douglas','osinski.mya@example.org','$2y$10$5.XvaBIeFZV7RHhu2F0bZOGH7mtAjjO1F0q.fUHEyjaj1kXgx3Gr2','0y4xPIjRBA','2017-04-30 22:38:54','2017-04-30 22:38:54'),(22,'Wilmer Gutkowski','renner.julien@example.com','$2y$10$vpJOScsu8bEnv7jUNvptkOGVxwtHKyeiNOnInnwYKCR6/rlEWmu5e','FeHUdkNF8X','2017-04-30 22:38:54','2017-04-30 22:38:54'),(23,'Martin Moen','pauline90@example.com','$2y$10$3g7jpvwLD1hud8c0OaFqgOVNU0.EdBc8pb0wlwZxezExzrLOjUXJK','rQarM6rI17','2017-04-30 22:38:54','2017-04-30 22:38:54'),(24,'Dr. Glenna Stamm','nrenner@example.com','$2y$10$iUURjR4kU9oicbzosFqyDeMGz4tuw5tmMfG4I6XUtJwd2EomLGyW2','xaUKdlkh2f','2017-04-30 22:38:54','2017-04-30 22:38:54'),(25,'Dr. Consuelo Douglas I','lynn52@example.com','$2y$10$hBvNpHzfPjoiW3tGni6yP..FTnrl67b7WAHXW2488biJVtHKKHuSi','X5KdkR74RV','2017-04-30 22:38:54','2017-04-30 22:38:54'),(26,'Gabriel Jenkins','lebsack.brody@example.com','$2y$10$.9v3gqAAkq.dcOIZAl82G.UiRajKbpdli.3xNd0nEipKIAIVYcFZ6','ib49UY7fOS','2017-04-30 22:38:54','2017-04-30 22:38:54'),(27,'Heloise Stiedemann','sydnie.lehner@example.org','$2y$10$GtDlZ7cNj8.4ZRKcnlVxR.Z9YPnYjkqs6hqqSXBuLicypyKuAwpvq','cF9qx3YXVJ','2017-04-30 22:38:54','2017-04-30 22:38:54'),(28,'Prof. Olen Moen','laisha.hane@example.com','$2y$10$HqmWZeSq5DQ31/VdwuvCNOQRUKpmszvx2Uyo5OfbGqfcK6pHc1B8C','BPn3xaGLBh','2017-04-30 22:38:54','2017-04-30 22:38:54'),(29,'Dr. Sammy Wilderman MD','vreichert@example.org','$2y$10$bbl9eq48qTmONcBF3J0kEukXhuWjA2UL8wmqdT7Bn/IOdo5n6rFdO','jaA7UYwjRe','2017-04-30 22:38:54','2017-04-30 22:38:54'),(30,'Mrs. Nova Jenkins IV','mgrimes@example.com','$2y$10$DQX.EXGmGge/VXuOeq.5zOidzzUN5rdqtS/nESpov5tVHc1OdXFDe','QFrvFYNwTw','2017-04-30 22:38:54','2017-04-30 22:38:54'),(31,'Dean Kuhn Jr.','lazaro.reinger@example.org','$2y$10$9Xh8HeEo5dm5BmwWaImEP.RhvpO4yMxOpd/PZwPNWAq4HxDiQmoAG','xAk4dTdnae','2017-04-30 22:38:54','2017-04-30 22:38:54'),(32,'Lemuel Herman PhD','brannon62@example.com','$2y$10$dptd9MZWfobqJDcfSoOP7.pPqK0zn18K5RbsANA2IJ9R1iB7FGRCO','D1pWZZgjyW','2017-04-30 22:38:54','2017-04-30 22:38:54'),(33,'Ashtyn Keebler','schoen.breanna@example.com','$2y$10$7HdxNEkEGBD9FAXIjD72M.4KZ3WimJpkO27LMNUdK.F/1j/YHRhki','Dts21cY9ky','2017-04-30 22:38:54','2017-04-30 22:38:54'),(34,'Jena Kautzer','west.sarai@example.org','$2y$10$dcSeOpB5O3BVEQYq3ttqzOJ9mFYbjv6ob19hNcmaXk9IgJ/qd5PRq','S85Bi1zqYB','2017-04-30 22:38:54','2017-04-30 22:38:54'),(35,'Dr. Leanna Goodwin','demarcus.huels@example.com','$2y$10$q2zsipCGzv3h8gVFYk3JdOi9xbaOUrPIzzl3DP61wEbJJFzusx4G.','dflUq2YCo8','2017-04-30 22:38:54','2017-04-30 22:38:54'),(36,'Dr. Margarett Lebsack I','camille.shields@example.org','$2y$10$eUaY4KJWfZc1Z0bx6JeBE./lrOqAbVcPncUpHgWLLo1wfOcMZSn6K','cAQeXKDSWE','2017-04-30 22:38:54','2017-04-30 22:38:54'),(37,'Mr. Xavier Kassulke Sr.','dianna.metz@example.org','$2y$10$rmRz46NjJJWQ8h48Zhjxl..kBLM0Ztm/ZnpRys4UvuN.ICla6oqya','pGxXE8qWJO','2017-04-30 22:38:54','2017-04-30 22:38:54'),(38,'Gilbert Hilpert IV','ohowe@example.org','$2y$10$iOiQtpYURS6g.ayGoqNRIO/KG3QKMa5JcveINf0g4yyEHxbBfcsTy','XCfvxEyR7E','2017-04-30 22:38:54','2017-04-30 22:38:54'),(39,'Dino Bashirian','frida.frami@example.net','$2y$10$z1oQtX.k8xmwYciornP7TeSLL00vi3OyvP6Ok7WW585aVt3GEm1yi','p2A9k8Xtlk','2017-04-30 22:38:54','2017-04-30 22:38:54'),(40,'Dudley Kuvalis','daniela34@example.com','$2y$10$3qWTz6wb0u1GXJ2JGbjks.OREGY/k3BUWxDGVKORLr6qciDTVqvoq','4VPW8UCtSG','2017-04-30 22:38:54','2017-04-30 22:38:54'),(41,'Micah Franecki','rodriguez.hilton@example.org','$2y$10$hR07b4D193.5Prub2r3JCOtR/RhLJH2VXKl1mJzcCcrZnPl8BokU6','6Wqho6vGUy','2017-04-30 22:38:54','2017-04-30 22:38:54'),(42,'Jerod Mante','gerald24@example.net','$2y$10$y8L1cRjw30O4/wFEDKYmsOhn3Md0E1j7wUG1yWKhhGQzXuwnq.aaq','Rvxrh2GoTt','2017-04-30 22:38:54','2017-04-30 22:38:54'),(43,'Sister Block','gertrude90@example.net','$2y$10$4fP1iRVH53bxOIVTICsxjunbP0Y4BEliu6FbUsGgaZKmdwm7aW2cG','GdWiudxy6G','2017-04-30 22:38:54','2017-04-30 22:38:54'),(44,'Madonna Rau III','sfunk@example.com','$2y$10$1VbeqQO2DJUzP.8Mbfpr1.2j5GIkU2.h6jSt725PJy1X7sUx0SImy','g3NZnMVX6z','2017-04-30 22:38:54','2017-04-30 22:38:54'),(45,'Isabella Daugherty','fay.emmitt@example.net','$2y$10$BdeiHOIbOe5HN3gYzXtOk.oT1S1.FE0IvHeIhtV9CMfuLVekNQZXa','289rIjjSA0','2017-04-30 22:38:54','2017-04-30 22:38:54'),(46,'Dr. Perry Walker','sabernathy@example.net','$2y$10$DC2BF.ZCiTyn.vXvLkn6deJJOOw1J/q4g3m8PTB7gCzfqFdwhppSa','1fISF2lywM','2017-04-30 22:38:54','2017-04-30 22:38:54'),(47,'Yasmeen Dibbert','zella64@example.com','$2y$10$mijjBnZWrA/DS2e71T8PMef6kExmBOMNZjx//y//2w7FDKvPfna4G','OLBcf0fwYw','2017-04-30 22:38:54','2017-04-30 22:38:54'),(48,'Raegan Mraz','coty53@example.net','$2y$10$7jrbqWZW0AFpaCHA.gLx..pRrM7JZBGe8Vc4Y4e0mxkm1rDS2dtL6','R1Xkmhu9j2','2017-04-30 22:38:54','2017-04-30 22:38:54'),(49,'Bryon Bogisich','cortney.glover@example.net','$2y$10$uZO7EI3eu3ebNTdropmxMutSaL1XJAGNaYwVdG0CKopF0R2bRDRau','etW8g43oSo','2017-04-30 22:38:54','2017-04-30 22:38:54'),(50,'Maximus Goldner','jordy.lueilwitz@example.com','$2y$10$vUes3J1MQj/ka5FlkCG5LeX3cnPyL6DaJNZXcN0fkybIlrkVrFsN6','InCVolVm2q','2017-04-30 22:38:54','2017-04-30 22:38:54'),(51,'Zelda Johnston','alex.shanahan@example.net','$2y$10$iaVvy8TNQ3wh4oe7Duhho.XkdkZ5O90eco1maA1qgIc.RNBRblO9W','LO0yqMKeQ2','2017-04-30 22:38:54','2017-04-30 22:38:54');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-01  0:54:01
