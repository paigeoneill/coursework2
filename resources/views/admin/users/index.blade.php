@extends('layouts.app')
<!-- styles the page -->

@section('content')

<h1>All Users</h1>

<h2>in order to edit a users role you need to add to the URL above. For example if editing user 1 to the end of the above url you need to add ‘\1\edit’</h2>

<section>
    @if (isset ($users))

        <table>
            <tr>
                <th>Username</th>
                <th>email</th>
                <th>Permissions</th>
            </tr>
            @foreach ($users as $user)
                <tr>
                    <td><a href="/admin/users/{{ $user->id }}" name="{{ $user->name }}">{{ $user->name }}</a></td>
                    <td> {{ $user->email }}</td>
                    <td>
                        <ul>
                            @foreach($user->roles as $role)
                                <li>{{ $role->label }}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
            @endforeach
        </table>
    @else
        <p>no users</p>
    @endif
</section>
@endsection