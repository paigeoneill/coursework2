@extends('layouts.app')
<!-- styles the page -->

@section('content')

    <!-- displays title -->
<h1 style="padding-left: 30px">Add Questionnaire</h1>


    <!-- creates form to display text boxes to create questionnaire -->
{!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createquestionnaire')) !!}
{{ csrf_field() }}
<div class="row large-12 columns" style="padding-left: 30px">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns" style="padding-left: 30px">
    {!! Form::label('description', 'Description (ethics):') !!}
    {!! Form::textarea('description', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-4 columns" style="padding-left: 30px">
    {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
</div>
{!! Form::close() !!}



<h1 style="padding-left: 30px">Add Question</h1>

    <!-- creates form to display text boxes to create question -->
{!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createquestion')) !!}
{{ csrf_field() }}
<div class="row large-12 columns" style="padding-left: 30px">
    {!! Form::label('question', 'Question:') !!}
    {!! Form::textarea('question', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns" style="padding-left: 30px">
    {!! Form::label('choice1', 'Possible answer:') !!}
    {!! Form::text('choice1', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns" style="padding-left: 30px">
    {!! Form::label('choice2', 'Possible answer:') !!}
    {!! Form::text('choice2', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns" style="padding-left: 30px">
    {!! Form::label('choice3', 'Possible answer:') !!}
    {!! Form::text('choice3', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns" style="padding-left: 30px">
    {!! Form::label('choice4', 'Possible answer:') !!}
    {!! Form::text('choice4', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns" style="padding-left: 30px">
    {!! Form::label('choice5', 'Possible answer:') !!}
    {!! Form::text('choice5', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns" style="padding-left: 30px">
    {!! Form::label('choice6', 'Possible answer:') !!}
    {!! Form::text('choice6', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-4 columns" style="padding-left: 30px">
    {!! Form::submit('Add Question', ['class' => 'button']) !!}
</div>
{!! Form::close() !!}
@endsection