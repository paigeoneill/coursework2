@extends('layouts.app')
<!-- styles the page -->

@section('content')

    <!-- displays questionnaire title, description and creator grabbing the details from the variables stored in the mysql database -->
<h1>{{ $questionnaire->title }}</h1>
<p>{{ $questionnaire->description }}</p>
<p>creator: {{ $article->user->name }}</p>
<p>Questions: </p>

<p class="flow-text center-align">Questions</p>

<ul class="collapsible" data-collapsible="expandable">
    @forelse ($questionnaire->questions as $question)
        <li style="box-shadow:none;">
        <div>{{ $question->title }}</div>
            {!! Form::open() !!}
            @foreach($question->choices as $key=>$val)
                <p style="margin:0px; padding:0px;">

                    <!-- create checkboxes to display answers-->
                    {!! Form::label('choices', 'Choices:') !!}
                    @foreach($questions as $question)
                        {{ Form::label($question->choices) }}
                        {{ Form::checkbox('choice[]', $question->id, $user->choices->contains($question->id), ['id' => $question->id]) }}
                    @endforeach

                    <input type="checkbox" id="{{ $key }}" />
                    <label for="{{$key}}">{{ $val }}</label>
                </p>
            @endforeach
            {!! Form::close() !!}
        </li>
    @empty
        <p>Nothing to show.</p>
    @endforelse
</ul>
@endsection
