@extends('layouts.app')
<!-- styles the page -->

@section('content')
<h1>Add Question</h1>

{!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createquestion')) !!}
{{ csrf_field() }}
<div class="row large-12 columns">
    {!! Form::label('question', 'Question:') !!}
    {!! Form::textarea('question', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
    {!! Form::label('choice1', 'Possible answer:') !!}
    {!! Form::text('choice1', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
    {!! Form::label('choice2', 'Possible answer:') !!}
    {!! Form::text('choice2', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
    {!! Form::label('choice3', 'Possible answer:') !!}
    {!! Form::text('choice3', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
    {!! Form::label('choice4', 'Possible answer:') !!}
    {!! Form::text('choice4', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
    {!! Form::label('choice5', 'Possible answer:') !!}
    {!! Form::text('choice5', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
    {!! Form::label('choice6', 'Possible answer:') !!}
    {!! Form::text('choice6', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-4 columns">
    {!! Form::submit('Add Question', ['class' => 'button']) !!}
</div>
{!! Form::close() !!}
@endsection



