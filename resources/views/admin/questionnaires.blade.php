@extends('layouts.app')
<!-- styles the page -->

@section('content')

<h1>Questionnaires</h1>

<section>
    @if (isset ($questionnaires))

        <ul>
            @foreach ($questionnaires as $questionnaire)
                <li><a href="/admin/questionnaires/{{ $questionnaire->id }}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li>
            @endforeach
        </ul>
    @else
        <p> no questionnaires added yet </p>
    @endif
</section>

{{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}
<div class="row">
<div class="row">
    {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
</div>
</div>
{!! Form::close() !!}
@endsection
