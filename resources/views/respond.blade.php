@extends('layouts.app')
<!-- styles the page -->

@section('content')

<h1>{{ $questionnaire->title }}</h1>
<p>{{ $questionnaire->description }}</p>
<p class="flow-text center-align">Questions</p>
<ul class="collapsible" data-collapsible="expandable">

    <!-- get question information from questionnaire chose-->
    @forelse ($questionnaire->questions as $question)
        <li style="box-shadow:none;">
            <div>{{ $question->title }}</div>

            <!-- create checkboxes to display answers-->
            {!! Form::open() !!}
            @foreach($question->choices as $key=>$val)
                <p style="margin:0px; padding:0px;">
                    <input type="checkbox" id="{{ $key }}" />
                    <label for="{{$key}}">{{ $val }}</label>
                </p>
            @endforeach
            {!! Form::close() !!}
        </li>
    @endforelse
</ul>

{!! Form::submit('Submit', ['class' => 'button']) !!}
</div>
{!! Form::close() !!}
@endsection